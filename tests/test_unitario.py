"""
modulo de pruebas unitarias
"""
import os
import pickle
import pytest
from google.api_core.exceptions import Forbidden
from unittest.mock import patch
import pandas as pd
import dag_functions2 as daf
import common_env as ce

@patch('dag_functions2.requests.get')
def test_extracion(mock_get):
    """
    prueba que puede obtener los datos de origen y almacenarlos localmente
    """
    mock_get.return_value.json.return_value = {
        "activity": "sample activity",
        "type": "sample type",
        "participants": 1
    }

    expected_output = {
        "file_exists": True,
        "records_number": 25
    }

    daf.obtener_datos()

    if os.listdir().count('datos') == 1:
        f_e = True
        lista_datos = pickle.load(open('datos', 'rb'))
        long_lista = len(lista_datos)
    else:
        f_e = False
        long_lista = 0

    actual_output = {
        "file_exists": f_e,
        "records_number": long_lista
    }

    assert expected_output == actual_output, 'Fallo en extaccion datos'

def test_transforma():
    """
    prueba que puede transformar los datos locales aplicando normalizacion
    """
    daf.transforma_datos()
    assert os.listdir().count('tipos.csv') == 1, 'Fallo en transformacion datos'

def test_carga():
    """
    prueba que puede subir los datos normalizados a destino
    """

    # mientras la credencial sea inválida
    with pytest.raises(Forbidden):
        
        daf.carga_datos()
        d_f= pd.read_gbq(
            f'select count(*) from {ce.DATASET_ID}.{ce.TIPOS_TABLE_ID}',
            project_id=ce.PROJECT_ID
            )
        assert d_f.shape[0] != 0, 'Fallo en carga datos'
