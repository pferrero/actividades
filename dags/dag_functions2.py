"""
2a versión de funciones para el DAG ETL
"""
import pickle
from datetime import datetime
import requests
import pandas as pd
import common_env as ce
import numpy as np
from sqlalchemy.engine import create_engine
from sqlalchemy.schema import DDL

def obtener_datos():
    """
    obtiene los datos de la api de actividades para su proceso posterior,
    y los almacena en una lista que se persiste con pickle
    """
    cant = 25
    endpoint = 'https://www.boredapi.com/api/activity'
    lista_salida = []
    hoy = datetime.date(datetime.now())

    for i in range(cant):
        resp = requests.get(endpoint)
        resp.raise_for_status()
        j = resp.json()
        lista_salida.append((hoy, j['activity'], j['type'], j['participants']))

    pickle.dump(lista_salida, open('datos', 'wb'))

    print(f'se obtuvieron {i} registros')

def transforma_datos():
    """
    obtiene del archivo pickle del paso anterior, y a partir de esos datos
    se normaliza y se exporta como csv
    """
    def first_index(la_lista :list, patron_bus :any) -> int:
        """
        obtiene el primer índice de un elemento en una lista
        """
        return np.where(la_lista==patron_bus)[0][0]

    lista_datos = pickle.load(open('datos', 'rb'))

    d_f = pd.DataFrame(lista_datos, columns=['fecha','actividad','tipo','participantes'])
    d_f.drop_duplicates(inplace=True)

    grupal = lambda x : x > 1
    d_f['grupal'] = grupal(d_f['participantes'])

    serie_tipos = d_f.tipo.unique()
    d_f['tipo'] = d_f['tipo'].apply(first_index, args=(serie_tipos, ))

    tipos = pd.DataFrame(serie_tipos, columns=['nombre'])
    tipos.index.rename('codigo', inplace=True)

    tipos.to_csv('tipos.csv', index=True)
    d_f.to_csv('acts.csv', index=False)
    print(f'hemos logrado determinar {tipos.shape[0]} tipos')

def carga_datos():
    """
    de los datos del csv del paso anterior, subo a GBQ la copia de dichos datos, 
    y genero 2 consultas en vistas en base a la info anterior
    """
    d_f = pd.read_csv('tipos.csv')
    f_d = pd.read_csv('acts.csv')

    d_f.to_gbq(
        destination_table= f'{ce.DATASET_ID}.{ce.TIPOS_TABLE_ID}',
        project_id= ce.PROJECT_ID,
        if_exists= 'replace'
        )

    f_d.to_gbq(
        destination_table= f'{ce.DATASET_ID}.{ce.ACTS_TABLE_ID}',
        project_id= ce.PROJECT_ID,
        if_exists= 'replace'
        )

    freq_act = f"""
        create or replace view {ce.DATASET_ID}.freq_act as
        SELECT nombre, count(*) cant
        FROM {ce.DATASET_ID}.{ce.TIPOS_TABLE_ID}
        join {ce.DATASET_ID}.{ce.ACTS_TABLE_ID} on tipo = codigo
        group by nombre
        order by 2 desc
        limit 5"""

    freq_grp = f"""
        create or replace view {ce.DATASET_ID}.freq_grp as
        SELECT grupal, count(*)
        FROM {ce.DATASET_ID}.{ce.ACTS_TABLE_ID}
        group by grupal"""

    engine = create_engine('bigquery://'+ce.PROJECT_ID)
    engine.execute(DDL(freq_act))
    engine.execute(DDL(freq_grp))
    engine.dispose()

    print('se ha subido el df a gbq')
