"""
ejemplo de dag para un ETL
"""
#pip install apache-airflow
from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.utils.dates import days_ago
import dag_functions2 as df

default_args = {
    'owner': 'airflow',
    'start_date': days_ago(2)
}

dag = DAG(
    dag_id= 'Ejemplo_de_ETL',
    description= 'DAG de un pipeline de ETL que obtiene datos de una api de \
        actividades, calcula la estadística de tipo, e intenta cargar el rdo en bq',
    schedule_interval= '0 10 * * *',
    default_args= default_args,
    catchup= False
)

extract = PythonOperator(
    task_id= 'taskE',
    python_callable= df.obtener_datos,
    dag= dag
)

transform = PythonOperator(
    task_id= 'taskT',
    python_callable= df.transforma_datos,
    dag= dag
)

load = PythonOperator(
    task_id = 'taskL',
    python_callable= df.carga_datos,
    dag= dag
)

extract >> transform >> load
