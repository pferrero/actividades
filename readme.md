# DATA ENGINEERING: Implementación de Normalización y ETL de un DW sobre Airflow y pipeline de CI sobre código Python

## Descripción General

Sobre la demostración CI, el proyecto implementa un pipeline de 2 comprobaciones: el linteo (adherencia a estándares de codificación) con Flake8, y las pruebas unitarias (refuerzan el QA) con PyTest. En la GUI de gitLab bajo Build->Trabajos pueden verse el rdo de las ejecuciones para cada commit. Cada cambio a la base de código que ingrese al repositorio pasará automáticamente por los test definidos para reforzar el QA del proyecto.


Sobre la demostración de Norm y ETL de DW en Airflow, en 1 DAG de 3 Tasks se intentará 
- obtener una lista de actividades de un API endpoint público atemporal en su etapa EXTRACT
- normalizar los datos obtenidos extrayendo la dimensión "tipo" y reemplazando el nombre original por el ID respectivo de la dimensión en su etapa TRANSFORM
- subir a GBQ las 2 tablas en su etapa LOAD

Ahora realizar y almacenar consultas como *"cuál es la frecuencia de cada tipo de actividad"* ó *"cuántas actividades grupales hay y cuántas no"* es más simple.

## Requerimientos de Ejecución

El archivo 'docker-compose.yml' contiene un volume y una env var personalizadas.
- el volume 'config' tiene como propósito almacenar el json de la credencial GCP
- la env var 'GOOGLE_APPLICATION_CREDENTIALS' debe contener la ruta y el nombre del archivo del parrafo anterior

El archivo '.gitignore' excluye a GIT los archivos de prueba unitaria en caso de haber sido usados y no borrados

El archivo '.gitlab-ci.yml' donde se define el pipeline con las 2 tareas de CI

## Despiece del desarrollo en 3 carpetas: 
- en dags\ 
  - el archivo 'etl_dag' implementando un ETL con 3 PythonOperator
  - el archivo 'dag_functions2' de código soporte al anterior 
  - el archivo 'common_env' con los ID de acceso al dataset en BigQuery
- en config\
  - el '*.json' correspondiente a la cuenta de servicio utilizada para el upload del paso LOAD
    - deberá ser remplazado por otro de igual estructura obtenido de la UI de GCP
- en tests\
  - el archivo 'test_unitario' que pretende validar el código de dag_functions2
  - el archivo 'requirements.txt' las dependencias de test_unitario
  - para su ejecución sera obligatorio la copia de los archivos ['dag_functions2','common_env'] a ésta carpeta antes de iniciar las pruebas

## Puesta en marcha
- Desplegar el multicontainer de Airflow 2.0 con 'docker-compose up -d' (para liberar la línea de comando tras el lanzamiento)
- Una vez disponible (en DockeDesktop se visualiza en ícono en naraja), acceder a la UI de AirFlow con la credencial estándar
- Sacar de pausa el dag y si el horario no está cerca del "schedule_interval", lanzarlo manualmente
- Una vez cocnluidos los Task con éxito, en el Dataset GBQ deberán visualizarse 4 objetos: 2 tablas: "activities" y "types", y 2 vistas "freq_act" y "freq_grp"

## Contacto
- Lic Pablo Esteban Ferrero
- email: peferrero@yahoo.com